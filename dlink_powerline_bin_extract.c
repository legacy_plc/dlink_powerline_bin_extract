/*
 * Copyright (c) 2021 bqgwzgqlhn
 *
 * Permission to use, copy, modify, and distribute this software for
 * any purpose with or without fee is hereby granted, provided that
 * the above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
 * OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>
#include <sys/stat.h>

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>

#define SIG_PACKAGE_DESC 0xcccccc12
#define SIG_PIB_FILE 0xcccccc90
#define SIG_NVM_FILE 0xcccccc91

static const int enable_debug = 0;

#define DBG_LOG(...) do { \
		if (enable_debug) { \
			printf(__VA_ARGS__); \
		} \
	} while (0)

#if 0
/* record format, not directly used by this program */
struct bin_record {
	uint32_t type;
	union {
		struct {
			uint32_t length;
			/* name is "length" bytes long */
			uint8_t name[];
		} pkg_desc;
		struct {
			uint32_t length;
			uint8_t name[128];
			/* File body is "length" bytes long */
			uint8_t file_body[];
		} file;
	};
} __attribute__((packed));
#else
struct record_hdr {
	uint32_t type;
	uint32_t length;
} __attribute__((packed));
#endif

#define READ_RECORD_FAIL -1
#define READ_RECORD_EOF 0
#define READ_RECORD_SUCCESS 1

int
read_record(int fd, void *v_buf, size_t amt)
{
	size_t bytes_read = 0;
	uint8_t *buf = v_buf;

	while (bytes_read < amt) {
		DBG_LOG("read %zu bytes so far, reading %zu more bytes\n",
			bytes_read, amt - bytes_read);

		ssize_t r = read(fd, buf + bytes_read, amt - bytes_read);
		if (r == -1 && errno == EINTR) {
			continue;
		}

		if (r == -1) {
			perror("read failed");
			return READ_RECORD_FAIL;
		} else if (r == 0) {
			return READ_RECORD_EOF;
		}

		DBG_LOG("just read %zu bytes\n", r);
		bytes_read += r;
	}

	return READ_RECORD_SUCCESS;
}

char *
process_package_desc(int fd, struct record_hdr *rec)
{
	size_t desc_len = rec->length;
	size_t buf_len = desc_len + 1;
	if (buf_len == 0) {
		return NULL;
	}

	char *pkg_name = malloc(buf_len);
	if (pkg_name == NULL) {
		return NULL;
	}

	int result = read_record(fd, pkg_name, desc_len);
	if (result != READ_RECORD_SUCCESS) {
		return NULL;
	}

	return pkg_name;
}

#define FILE_NAME_STR_LENGTH 128

int
write_file_record(int fd, size_t file_length, const char *filename, int out_fd)
{
	size_t bytes_read = 0;
	uint8_t buf[1024];

	while (bytes_read < file_length) {
		size_t bytes_to_read = file_length - bytes_read;
		if (bytes_to_read > sizeof(buf)) {
			bytes_to_read = sizeof(buf);
		}

		DBG_LOG("read %zu bytes so far, reading %zu more bytes\n",
			bytes_read, bytes_to_read);

		/* read chunk */
		ssize_t r = read(fd, buf, bytes_to_read);
		if (r == -1 && errno == EINTR) {
			continue;
		}
		if (r == -1) {
			perror("read failed while extracting file");
			return -1;
		} else if (r == 0) {
			fprintf(stderr, "error: hit unexpected end of file while extracting \"%s\"!", filename);
			return -1;
		}

		DBG_LOG("just read %zu bytes\n", r);
		bytes_read += r;

		size_t bytes_to_write = r;

		/* write all bytes */
		while (bytes_to_write) {
			DBG_LOG("writing %zu bytes to fd %d!\n", bytes_to_write, out_fd);

			r = write(out_fd, buf, bytes_to_write);
			if (r == -1 && errno == EINTR) {
				continue;
			}
			if (r == -1) {
				perror("extracting file failed");
				return -1;
			}

			DBG_LOG("just wrote %zu bytes\n", r);

			bytes_to_write -= r;
		}
	}

	return 0;
}

int
process_file_record(int fd, struct record_hdr *rec)
{
	struct stat stbuf;
	char filename[FILE_NAME_STR_LENGTH];
	uint32_t file_len = rec->length;
	int r;

	r = read_record(fd, filename, FILE_NAME_STR_LENGTH);
	if (r != READ_RECORD_SUCCESS) {
		fprintf(stderr, "error: could not read record filename!\n");
		return -1;
	}
	filename[FILE_NAME_STR_LENGTH - 1] = '\0';

	r = stat(filename, &stbuf);
	if (r != -1) {
		fprintf(stderr, "error: refusing to overwrite existing file %s!\n", filename);
		return -1;
	}

	printf("Extracting file \"%s\" (%u bytes)...\n", filename, file_len);

	int out_fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC,
		S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

	if (out_fd == -1) {
		perror("failed to open output file!");
		return -1;
	}

	r = write_file_record(fd, file_len, filename, out_fd);
	if (r == -1) {
		fprintf(stderr, "error: failed to write file \"%s\"!", filename);
		close(out_fd);
		return -1;
	}

	close(out_fd);

	return 0;
}

int
process_record(int fd, struct record_hdr *rec)
{
	char *pkg_name = NULL;
	int r;

	switch (rec->type) {
	case SIG_PACKAGE_DESC:
		DBG_LOG("processing package description record!\n");
		pkg_name = process_package_desc(fd, rec);
		if (pkg_name == NULL) {
			fprintf(stderr, "error: failed to extract package name\n");
			return -1;
		}
		printf("Extracting package %s...\n", pkg_name);
		free(pkg_name);
		break;
	case SIG_PIB_FILE:
	case SIG_NVM_FILE:
		DBG_LOG("processing file record: 0x%08x\n", rec->type);
		r = process_file_record(fd, rec);
		if (r == -1) {
			fprintf(stderr, "error: failed to process file record!\n");
			return -1;
		}
		break;
	default:
		fprintf(stderr, "error: unknown record type: 0x%08x\n", rec->type);
		return -1;
	}

	return 0;
}

int
process_records(int fd)
{
	struct record_hdr rec;
	int result;

	while ((result = read_record(fd, &rec, sizeof(rec))) == READ_RECORD_SUCCESS) {
		result = process_record(fd, &rec);
		if (result == -1) {
			return -1;
		}
	}

	if (result == READ_RECORD_FAIL) {
		fprintf(stderr, "failed to one or more read record!\n");
		return -1;
	}

	return 0;
}

int
main(int argc, char **argv)
{
	if (argc != 2) {
		fprintf(stderr, "usage: %s dlink_bin_file.bin\n", argv[0]);
		exit(1);
	}

	const char *bin_file = argv[1];

	int fd = open(bin_file, O_RDONLY);
	if (fd == -1) {
		perror("could not open bin file");
		exit(1);
	}

	int result = process_records(fd);
	if (result == -1) {
		fprintf(stderr, "error: failed to process all records!\n");
		exit(1);
	}

	return 0;
}
