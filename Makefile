CC=gcc
CFLAGS=-Wall -Wextra

all: dlink_powerline_bin_extract

dlink_powerline_bin_extract: dlink_powerline_bin_extract.c
	$(CC) -o $@ $^ $(CFLAGS)

clean:
	rm -f dlink_powerline_bin_extract
