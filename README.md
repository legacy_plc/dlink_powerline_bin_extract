# Legacy D-Link Powerline Binary Extractor

> :warning: **Flashing a powerline device may destroy it and void your warranty! Use at your own risk!** :warning:

Extracts PIB and NVM files from D-Link Powerline .BIN files for legacy
powerline adapters such as the [DHP-540](https://support.dlink.ca/ProductInfo.aspx?m=DHP-540).

Example BIN file: https://ftp.dlink.ca/ftp/PRODUCTS/DHP-540/DHP-540_FIRMWARE_1.02B03.BIN

## Building

```console
$ make
gcc -o dlink_powerline_bin_extract dlink_powerline_bin_extract.c -Wall -Wextra
```

## Usage Example

```console
$ ./dlink_powerline_bin_extract DHP-540_FIRMWARE_1.02B03.BIN 
Extracting package DHP540A2_FW102b03_FWDate_22_Jul_2011_CS_1B2150FE...
Extracting file "DHP540A2_PIB106CEB_DT.pib" (16352 bytes)...
Extracting file "DHP540A2_PIB109NA_DT.pib" (16352 bytes)...
Extracting file "ar7400-v5.2.0-01-3-X-FINAL.nvm" (456976 bytes)...
```

Be sure to check the PIB and NVM files for validity, using [open-plc-utils](https://github.com/qca/open-plc-utils):

```console
$ chkpib DHP540A2_PIB106CEB_DT.pib DHP540A2_PIB109NA_DT.pib
DHP540A2_PIB106CEB_DT.pib looks good
DHP540A2_PIB109NA_DT.pib looks good

$ chknvm ar7400-v5.2.0-01-3-X-FINAL.nvm
file ar7400-v5.2.0-01-3-X-FINAL.nvm looks good
```

## License
```c
/*
 * Copyright (c) 2021 bqgwzgqlhn
 *
 * Permission to use, copy, modify, and distribute this software for
 * any purpose with or without fee is hereby granted, provided that
 * the above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
 * OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
```
